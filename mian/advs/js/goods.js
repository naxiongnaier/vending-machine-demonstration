// 统计页面js文件
window.addEventListener('load', function () {
    // 《1》第一步，上面模块选项卡，点击某一个改变其样式，其余不变
    var btn = document.querySelector('.demo').querySelectorAll('div');
    var items = document.querySelector('.main').querySelectorAll('.item');
    for (var i = 0; i < btn.length; i++) {
        // 《2》第二步，将上面选项卡添加自定义属性，属性值从0开始
        btn[i].setAttribute('data-index', i);
        btn[i].addEventListener('click', function () {
            // 清除全部元素的样式
            for (var j = 0; j < btn.length; j++) {
                btn[j].style.backgroundColor = '#fff';
                btn[j].style.color = '#333';
            }
            this.style.color = '#fff';
            this.style.backgroundColor = '#019fe6';

            // 清除全部子选项卡的display样式
            for (var z = 0; z < items.length; z++) {
                items[z].style.display = 'none';
            }
            // 获取给当前父选项卡的编号，让对应子选项卡恢复display
            items[this.getAttribute('data-index')].style.display = 'block';
        });
    };

    // 更改超链接跳转
    var link = document.querySelector('.footer');
    btn[0].addEventListener('click', function () {
        link.href = './add_goods.html';
    });
    btn[1].addEventListener('click', function () {
        link.href = './add_plan.html';
    });

    // 代理设置功能
    var open_btn1 = document.querySelector('.main').querySelector('#content_1').querySelector('.template1');
    var open_btn2 = document.querySelector('.main').querySelector('#content_2').querySelector('.plan');
    var data1 = document.querySelector('.main').querySelector('#content_1').querySelector('.set');
    var data2 = document.querySelector('.main').querySelector('#content_2').querySelector('.set');
    var close_btn1 = document.querySelector('.main').querySelector('#content_1').querySelector('.set').querySelector('.li3');
    var close_btn2 = document.querySelector('.main').querySelector('#content_2').querySelector('.set').querySelector('.li5');
    open_btn1.addEventListener('click', function () {
        data1.style.display = 'block';
    });
    open_btn2.addEventListener('click', function () {
        data2.style.display = 'block';
    });
    close_btn1.addEventListener('click', function () {
        data1.style.display = 'none';
        console.log('点击了');
    });
    close_btn2.addEventListener('click', function () {
        data2.style.display = 'none';
        console.log('点击了');
    });
});