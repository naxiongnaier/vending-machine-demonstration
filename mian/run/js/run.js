// 统计页面js文件
window.addEventListener('load', function () {
    // 《1》第一步，上面模块选项卡，点击某一个改变其样式，其余不变
    var btn = document.querySelector('.demo').querySelectorAll('div');
    var items = document.querySelector('.main').querySelectorAll('.item');
    for (var i = 0; i < btn.length; i++) {
        // 《2》第二步，将上面选项卡添加自定义属性，属性值从0开始
        btn[i].setAttribute('data-index', i);
        btn[i].addEventListener('click', function () {
            // 清除全部元素的样式
            for (var j = 0; j < btn.length; j++) {
                btn[j].style.backgroundColor = '#dfdfdf';
                btn[j].style.color = '#333';
            }
            this.style.color = '#019fe6';
            this.style.backgroundColor = '#a3c3d1';

            // 清除全部子选项卡的display样式
            for (var z = 0; z < items.length; z++) {
                items[z].style.display = 'none';
            }
            // 获取给当前父选项卡的编号，让对应子选项卡恢复display
            items[this.getAttribute('data-index')].style.display = 'block';
        });
    };
});