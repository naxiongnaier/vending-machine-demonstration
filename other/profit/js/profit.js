// 统计页面js文件
window.addEventListener('load', function () {
    // 《1》第一步，上面模块选项卡，点击某一个改变其样式，其余不变
    var btn_on = document.querySelector('.chart').querySelector('.title');
    var item = document.querySelector('.condition');
    var btn_off = document.querySelector('.condition').querySelector('.content').querySelector('.item2');
    console.log(btn_on);
    btn_on.addEventListener('click', function () {
        item.style.display = 'block';
    });
    btn_off.addEventListener('click', function () {
        item.style.display = 'none';
    });
});